analyse-d-images

Le rapport est dans le fichier pdf à la racine du git

Le dossier CBIR-master contient les fichiers utilisés pour la classification par critères.
Seuls les fichiers color.py, DB2.py (remplaçant DB.py) et evaluate.py ont été utilisés.

Le dossier NeuralNetwork contient les fichiers utilisés pour constituer mon réseau de neurones.
Les 3 fichiers .h5 sont les trois modèles que j'ai générés avec des valeurs différentes pour le réseau de neurones.

Afin de ne pas inonder d'images le git, je n'ai pas mis les images que j'ai utilisées lors de ce projet. Vous trouverez cependant dans mon rapport le nom des classes que j'ai utilisées.
Je les remet également ici pour plus de clarté :
 - wl_buttrfly
 - wl_deer
 - wl_cat
 - wl_eagle
 - wl_cougr
