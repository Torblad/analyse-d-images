# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 08:18:38 2020

@author: Simon
"""

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K
import os

from keras.callbacks import Callback, EarlyStopping


'''
assert len(train_input) != 0, "dataset must not be empty"

np_input = np.asarray(train_input)

class TrainingHistory(Callback):
    def __init__(self):
        super(TrainingHistory, self).__init__()

        self.losses = None
        self.predictions = None
        self.i = None
        self.save_every = None

    def on_train_begin(self, logs=None):
        self.losses = []
        self.predictions = []
        self.i = 0
        self.save_every = 50

    def on_epoch_end(self, batch, logs=None):
        self.losses.append(logs.get('loss'))
        self.i += 1
        if self.i % self.save_every == 0:
            pred = self.model.predict(np_input)
            self.predictions.append(pred)
'''

# dimensions of our images.
img_width, img_height = 150, 150

train_data_dir = 'data/train'
validation_data_dir = 'data/validation'

nbClasses = sum([len(d) for r, d, files in os.walk(train_data_dir)])
nb_train_samples = sum([len(files) for r, d, files in os.walk(train_data_dir)]) #5000
nb_validation_samples = sum([len(files) for r, d, files in os.walk(validation_data_dir)]) #2000
epochs = 100
batch_size = 20

if K.image_data_format() == 'channels_first':
    input_shape = (3, img_width, img_height)
else:
    input_shape = (img_width, img_height, 3)


#history = TrainingHistory()
early_stopping = EarlyStopping(monitor="val_loss", patience=5, verbose=0)

model = Sequential(layers=[
    Conv2D(64, (3, 3), input_shape=input_shape, activation='relu'),
    MaxPooling2D(pool_size=(2, 2)),
    Conv2D(64, (3, 3), activation='relu'),
    MaxPooling2D(pool_size=(2, 2)),
    Conv2D(128, (3, 3), activation='relu'),
    MaxPooling2D(pool_size=(2, 2)),
    Flatten(),
    Dense(units=16, activation='relu'),
    #Dropout(0.5),
    Dense(units=nbClasses, activation='sigmoid')
])
''' 


model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(64))
model.add(Activation('relu'))

model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))
'''

model.compile(loss='categorical_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

# this is the augmentation configuration we will use for training
train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

# this is the augmentation configuration we will use for testing:
# only rescaling
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='categorical')

validation_generator = test_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='categorical')

model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=nb_validation_samples // batch_size,
    callbacks=[early_stopping])

model.save_weights('categorical_crossentropy_learning_5classes.h5')


def reports(y_pred, y_true, class_name):
    classification = classification_report(y_true, y_pred, target_names=class_name)
    confusion = confusion_matrix(y_true, y_pred)
    
    return classification, confusion

